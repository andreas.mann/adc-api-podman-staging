#!/bin/bash
SCRIPT_DIR=`dirname "$0"`

SCRIPT_DIR_FULL="$( readlink -f ${SCRIPT_DIR}  )";
POD_CONF_FILE="podman-adcapi.conf.sh"
# read configuration for turnkey pod
. $SCRIPT_DIR_FULL/$POD_CONF_FILE

podman exec -it --user root http${ADCAPI_DB_CONT_NAME_SUFFIX} /bin/sh
