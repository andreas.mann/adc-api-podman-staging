#!/bin/bash
SCRIPT_DIR=`dirname "$0"`

SCRIPT_DIR_FULL="$( readlink -f ${SCRIPT_DIR}  )";
POD_CONF_FILE="podman-adcapi.conf.sh"
# read configuration for turnkey pod
. $SCRIPT_DIR_FULL/$POD_CONF_FILE

# check number of arguments
NB_ARGS=3
if [ $# -lt $NB_ARGS ];
then
    echo "$0: wrong number of arguments ($# instead of at least $NB_ARGS)"
    echo "usage: $0 db_name bz2_file_to_restore run_custom_sql_file"
	echo "e.g. scriptspod/restore_database_adc-api.sh athero_w06_slice1_run4_rebased dump-athero_w06_slice1_run4_rebased-2022-03-10.sql.bz2 true"
    exit 1
fi


# file name of bz2 file, dump-athero_w06_slice1_run4_rebased-2022-03-10.sql.bz2 
restore_db_name=$1
restore_abs_file_path=$2

restore_with_custom_sql_applied="with-custom-sql";
if [ -z $3 ];then
	# empty
	restore_with_custom_sql_applied="false";
else
	restore_with_custom_sql_applied=$3;
fi

echo "CHECK: !!!you can only restore databases, which are configured in $POD_CONF_FILE"
echo "CHECK dump file: ${restore_abs_file_path}"
echo "CHECK: run custom sql file: ${restore_with_custom_sql_applied}"
echo "CHECK: databases to restore: ${restore_db_name}"
echo "CHECK: current databases:"
echo "--------------------"
#podman exec --log-level debug -i --user root maria_db${ADCAPI_DB_CONT_NAME_SUFFIX} /bin/bash -c "mysql -h 127.0.0.1 -u root -p${ADCAPI_MARIADB_ROOT_PASSWORD} --execute='SHOW DATABASES;'"
podman exec -i --user root maria_db${ADCAPI_DB_CONT_NAME_SUFFIX} /bin/bash -c "mysql -h 127.0.0.1 -u root -p${ADCAPI_MARIADB_ROOT_PASSWORD} --execute='SHOW DATABASES;'"
echo "--------------------"
echo "${restore_db_name} will be restored with ${restore_abs_file_path} "
echo "all ok: y/n?"
read input
if [ $input != "y" ]; then
	exit
fi

log "${restore_db_name} will be restored with ${restore_abs_file_path}"

parent_dir="$(dirname "$restore_abs_file_path")"
base_name="$(basename "$restore_abs_file_path")"
 

#echo "path: $SCRIPT_DIR_FULL/$POD_CONF_FILE "

echo "restore_abs_file_path:  $restore_abs_file_path"
echo "restore_with_custom_sql_applied: $restore_with_custom_sql_applied"
if [ -z $restore_abs_file_path ];then
	echo "filename to restore cannot be empty!"
	exit;
fi 

# import data
log "restore data from ${PATH_BKUP_DIR}/$ADCAPI_POD_NAME_SVC/restore/${restore_abs_file_path}"
tmp_path_bz2="${PATH_BKUP_DIR}/$ADCAPI_POD_NAME_SVC/restore/${restore_abs_file_path}"
echo "full path to restore file: tmp_path_bz2"
echo "decompressing ..."
bzip2 -v -d -k $tmp_path_bz2
suffix=".bz2";
path_to_sql_dump=${tmp_path_bz2%"$suffix"}
echo "--path to sql dump- ${path_to_sql_dump}"
prefix1="${PATH_BKUP_DIR}/$ADCAPI_POD_NAME_SVC/restore/"
ptsd1=${path_to_sql_dump#"$prefix1"}
echo "--sql file- ${ptsd1}"
# /mnt/bkup/restore
echo "--internal path- /mnt/bkup/restore/${ptsd1}"
podman exec --log-level debug -i --user root maria_db${ADCAPI_DB_CONT_NAME_SUFFIX} /bin/bash -c "mysql -h 127.0.0.1 -u root -p${ADCAPI_MARIADB_ROOT_PASSWORD} ${restore_db_name} < /mnt/bkup/restore/${ptsd1}"

# run custom sql file
if [ $restore_with_custom_sql_applied = "true"  ];then
	tmp_custom_file=${ADCAPI_MARIADB_DBREPO_IMPORTFILE_LIST_ASS["${restore_db_name}"]}
	log "import custom sql file $tmp_custom_file"
	if [ -n "${tmp_custom_file}" ];then
			log "--- ${tmp_custom_file}"
			 podman exec --log-level debug -i --user root maria_db${ADCAPI_DB_CONT_NAME_SUFFIX} /bin/bash -c "mysql -h 127.0.0.1 -u root -p${ADCAPI_MARIADB_ROOT_PASSWORD} ${restore_db_name} < /mnt/data/conf/${tmp_custom_file}"
	fi
fi
echo "remove sql file"
rm -f $path_to_sql_dump
echo "FIN"

