#!/bin/bash
#
# installs keycloak service as pod from adc-middleware 
# see https://github.com/ireceptorplus-inesctec/adc-middleware
#
# -----------------------------------------------------------------------------

SCRIPT_DIR=`dirname "$0"`
#SCRIPT_DIR_FULL="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
SCRIPT_DIR_FULL="$( readlink -f ${SCRIPT_DIR}  )";
POD_CONF_FILE="podman-adcapi.conf.sh" 


if [ ! -f "$SCRIPT_DIR/$POD_CONF_FILE" ]; then
    echo "The file $POD_CONF_FILE does not exist. Edit file podman-adcapi.conf.sh.EDIT then remove .EDIT"
	exit
fi

# read configuration for turnkey pod
. $SCRIPT_DIR_FULL/$POD_CONF_FILE 

echo "CHECK: -- ?"
echo "CHECK: loginctl enable-linger for ${POD_USER_ID}" && loginctl show-user | grep KillUserProcesses
echo "CHECK: nginx running ?"
#echo "CHECK: IP_LAN_DEVICE OK (set to ${IP_LAN_DEVICE}) ?"
echo "check image: ${ADCAPI_HTTP_IMAGE}:${ADCAPI_HTTP_IMAGE_TAG}" && podman images | grep ${ADCAPI_HTTP_IMAGE}
echo "check hosts file: " && cat /etc/hosts
echo "all ok: y/n?"
read input
if [ $input != "y"  ]; then 
	exit
fi

#echo --- BIN_PODMAN  ---  $PODMAN_CMD
log "[$ADCADPI_POD_NAME_SVC installation]"
log "pod service name: $ADCAPI_POD_NAME_SVC"
log "pod external port: $ADCAPI_POD_EX_PORT_HTTP"
log "user id running db container: $POD_USER_ID"

# required commands
commands=("podman" "curl" "readlink" "buildah" "unzip" "bzip2");
for i in "${commands[@]}"
do
	if ! [ $(command -v $i) > 0 ];then
		log "	$i command could not be found, install first. "
		exit
	else
		log "	$i command available"		
	fi
done

# db data folder
ADCAPI_DB_DATA_DIR=".maria_db_data"
log "create $ADCAPI_DB_DATA_DIR and bkup folders"
bkup_folder_list=("incoming" "backup.daily" "backup.weekly" "backup.monthly" "restore");
if [ ! -e $ADCAPI_DB_DATA_DIR  ];then

	mkdir $ADCAPI_DB_DATA_DIR >> $SCRIPT_DIR/$LOGFILE_NAME 
	podman unshare chown $POD_USER_ID:$POD_USER_ID -R $ADCAPI_DB_DATA_DIR

	mkdir -p ${PATH_BKUP_DIR}/$ADCAPI_POD_NAME_SVC
	for b in "${bkup_folder_list[@]}"
	do
		mkdir -p ${PATH_BKUP_DIR}/$ADCAPI_POD_NAME_SVC/$b >> $SCRIPT_DIR/$LOGFILE_NAME
		#
	done

else
	log "	data directory for db already exists"
	exit
fi

log "check for existing pod.."
podman pod exists $ADCAPI_POD_NAME_SVC 
if [ $? -eq 0  ]; then
	log "	pod with that name already exists."
	exit
fi

# --- pull containers ---------------------------------------------------------
log "### pull containers ------------------------------------------------------"
# build container image
# https://hub.docker.com/r/jboss/keycloak/tags
# docker pull jboss/keycloak:15.0.0
# Dockerfile
# FROM jboss/keycloak:15.0.0
# COPY ./js-policies /opt/jboss/keycloak/standalone/deployments
#podman pull registry.hub.docker.com/jboss/keycloak:
podman pull docker.io/library/mariadb:${ADCAPI_DB_CONT_TAG} 
sleep 2
# postgres:12

#podman pull docker.io/library/postgres:${KEYCL_DB_CONT_TAG} 

if [[ ${ADCAPI_HTTP_IMAGE} == *"localhost"* ]]; then
    log "using local adc-api http image"
else
	log "pull http image .."
	podman pull ${ADCAPI_HTTP_IMAGE}:${ADCAPI_HTTP_IMAGE_TAG}
fi

sleep 2

# --- pod create --------------------------------------------------------------

log "### creating the adcapi pod ---------------------------------------------"
podman pod create --name $ADCAPI_POD_NAME_SVC --share net -p $ADCAPI_POD_EX_PORT_HTTP:51431 -p $ADCAPI_POD_EX_PORT_DB:3306

log "add container maria_db"
podman run --user $POD_USER_ID -d --pod=$ADCAPI_POD_NAME_SVC  \
            -e MARIADB_DATABASE=$ADCAPI_MARIADB_DATABASE \
			-e MARIADB_ROOT_PASSWORD=$ADCAPI_MARIADB_ROOT_PASSWORD \
			-v $ADCAPI_DB_DATA_DIR:/var/lib/mysql:Z \
			-v ${PATH_BKUP_DIR}/$ADCAPI_POD_NAME_SVC:/mnt/bkup:Z \
			-v ${ADCAPI_MARIADB_DATA_PATH}:/mnt/data:Z \
            --name=maria_db${ADCAPI_DB_CONT_NAME_SUFFIX} \
			docker.io/library/mariadb:$ADCAPI_DB_CONT_TAG

wait "run maria_db" 10

# create bkup folder chown not required 
log "create bkup, data .."
# root@98106decdb80:/mnt/bkup# pg_dump -Fc keycloak_db -U postgres -h localhost > /mnt/bkup/incoming/postgres_legolas_adc-keycloak-LEGOLAS-dev_211206134500.sql.dump
podman exec -d --user root maria_db${ADCAPI_DB_CONT_NAME_SUFFIX} sh  "cd /mnt && mkdir -p bkup && chown mysql bkup"
podman exec -d --user root maria_db${ADCAPI_DB_CONT_NAME_SUFFIX} sh  "cd /mnt && mkdir -p data && chown mysql data"
sleep 2

# is set then copy custom cnf"
if [ -n "${ADCAPI_MARIADB_CUSTOM_CNF}" ];then
	log "copy custom cnf file"
	podman exec -d --user root maria_db${ADCAPI_DB_CONT_NAME_SUFFIX} /bin/bash -c "cd /mnt/data/conf && cp /mnt/data/conf/${ADCAPI_MARIADB_CUSTOM_CNF} /etc/mysql/conf.d"
	echo $?
fi
sleep 1

# create 2nd super user
log "create 2nd super user"
rm -f ${ADCAPI_MARIADB_DATA_PATH}/2nd_super_user.sql
echo "GRANT ALL PRIVILEGES ON *.* TO 'root'@'127.0.0.1' WITH GRANT OPTION;" >> ${ADCAPI_MARIADB_DATA_PATH}/2nd_super_user.sql
echo "CREATE USER '${ADCAPI_MARIADB_DBADMIN}'@'%' IDENTIFIED BY '${ADCAPI_MARIADB_DBADMIN_PWD}';" >> ${ADCAPI_MARIADB_DATA_PATH}/2nd_super_user.sql
echo "GRANT ALL PRIVILEGES ON *.* TO '${ADCAPI_MARIADB_DBADMIN}'@'%' WITH GRANT OPTION;" >> ${ADCAPI_MARIADB_DATA_PATH}/2nd_super_user.sql
echo "FLUSH PRIVILEGES;" >> ${ADCAPI_MARIADB_DATA_PATH}/2nd_super_user.sql
podman exec -d --user root maria_db${ADCAPI_DB_CONT_NAME_SUFFIX} /bin/bash -c "mysql -h 127.0.0.1 -u root -p${ADCAPI_MARIADB_ROOT_PASSWORD} mysql < /mnt/data/2nd_super_user.sql"

# create databases
log "create databases .."
for i in ${ADCAPI_MARIADB_DB_LIST[@]}; do
	log "-- ${i}";
	tmp_pwd=${ADCAPI_MARIADB_DB_PWD_LIST_ASS["$i"]}
	echo "--- ${tmp_pwd}"
	podman exec -d --user root maria_db${ADCAPI_DB_CONT_NAME_SUFFIX} \
		 /bin/bash -c "mysql -h 127.0.0.1 -u root -p${ADCAPI_MARIADB_ROOT_PASSWORD} -e \"CREATE DATABASE ${i} CHARACTER SET utf8 COLLATE utf8_general_ci;\""
	podman exec -d --user root maria_db${ADCAPI_DB_CONT_NAME_SUFFIX} \
		 /bin/bash -c "mysql -h 127.0.0.1 -u root -p${ADCAPI_MARIADB_ROOT_PASSWORD} -e \"GRANT ALL PRIVILEGES ON ${i}.* To '${i}'@'%' IDENTIFIED BY '${tmp_pwd}';\""
	podman exec -d --user root maria_db${ADCAPI_DB_CONT_NAME_SUFFIX} \
		 /bin/bash -c "mysql -h 127.0.0.1 -u root -p${ADCAPI_MARIADB_ROOT_PASSWORD} -e \"GRANT ALL PRIVILEGES ON ${i}.* To '${i}'@'127.0.0.1' IDENTIFIED BY '${tmp_pwd}';\""
	echo $?
	podman exec -d --user root maria_db${ADCAPI_DB_CONT_NAME_SUFFIX} \
		 /bin/bash -c "mysql -h 127.0.0.1 -u root -p${ADCAPI_MARIADB_ROOT_PASSWORD} -e \"GRANT ALL PRIVILEGES ON ${i}.* To '${ADCAPI_MARIADB_DBADMIN_SCI}'@'%' IDENTIFIED BY '${ADCAPI_MARIADB_DBADMIN_SCI_PWD}';\""
	podman exec -d --user root maria_db${ADCAPI_DB_CONT_NAME_SUFFIX} \
		 /bin/bash -c "mysql -h 127.0.0.1 -u root -p${ADCAPI_MARIADB_ROOT_PASSWORD} -e \"GRANT ALL PRIVILEGES ON ${i}.* To '${ADCAPI_MARIADB_DBADMIN_SCI}'@'127.0.0.1' IDENTIFIED BY '${ADCAPI_MARIADB_DBADMIN_SCI_PWD}';\""
	sleep 1
done

sleep 2

# ---------------------------------------
do_import="true";
if [ "$do_import" = "true" ]; then

# import data
log "import data"
for i in ${ADCAPI_MARIADB_DB_LIST[@]}; do
	log "-- ${i}";
	tmp_path_bz2=${ADCAPI_MARIADB_DB_DUMPFILE_LIST_ASS["$i"]}
	echo "--path to bz2- ${tmp_path_bz2}"
	bzip2 -d -k $tmp_path_bz2
	suffix=".bz2";	
	path_to_sql_dump=${tmp_path_bz2%"$suffix"}
	echo "--path to sql dump- ${path_to_sql_dump}"
	prefix1="${ADCAPI_MARIADB_BZ2DUMP_PATH}/"
	ptsd1=${path_to_sql_dump#"$prefix1"}
	echo "--sql file- ${ptsd1}"
	echo "--internal path- ${ADCAPI_MARIADB_CONT_SQLDUMP_PATH}/${ptsd1}" 
	podman exec --log-level debug -i --user root maria_db${ADCAPI_DB_CONT_NAME_SUFFIX} /bin/bash -c "mysql -h 127.0.0.1 -u root -p${ADCAPI_MARIADB_ROOT_PASSWORD} ${i} < ${ADCAPI_MARIADB_CONT_SQLDUMP_PATH}/${ptsd1}"
	
	sleep 2
done

sleep 2

# run custom sql file
log "import custom sql file"
for dbrep in ${ADCAPI_MARIADB_DBREPO_LIST[@]}; do
	log "-- ${dbrep}"
	tmp_custom_file=${ADCAPI_MARIADB_DBREPO_IMPORTFILE_LIST_ASS["$dbrep"]}
    # /mnt/data/conf/${tmp_custom_file}
	if [ -n "${tmp_custom_file}" ];then
		log "--- ${tmp_custom_file}"
		podman exec --log-level debug -i --user root maria_db${ADCAPI_DB_CONT_NAME_SUFFIX} /bin/bash -c "mysql -h 127.0.0.1 -u root -p${ADCAPI_MARIADB_ROOT_PASSWORD} ${dbrep} < /mnt/data/conf/${tmp_custom_file}"
	fi

	sleep 2
done

fi
# ---------------------------------------

log  "restart db container ..."
podman stop maria_db${ADCAPI_DB_CONT_NAME_SUFFIX}
sleep 2
podman start maria_db${ADCAPI_DB_CONT_NAME_SUFFIX}
wait "run maria_db" 10

# ---------------------------------------
log "start http container"
# run --user 1000 -d --pod=adc-api-LEGOLAS-adcapi-220511 --name=http-LEGOLAS-adcapi-220511 localhost/adc-api-alpine:220511
podman run --user $POD_USER_ID -d --pod=$ADCAPI_POD_NAME_SVC  \
		--name=http${ADCAPI_DB_CONT_NAME_SUFFIX} \
		${ADCAPI_HTTP_IMAGE}:${ADCAPI_HTTP_IMAGE_TAG}

wait "container http-${ADCAPI_DB_CONT_NAME_SUFFIX} starting" 4


# --- create systemd service --------------------------------------------------
log "install systemd service --------------------------------------------------"
# launch on boot
# adc-middleware/pod-adc-auth-keycloak-test1.service
# adc-middleware/container-keycloak-test1.service
# adc-middleware/container-keycloak_db-test1.service

mkdir -p ~/.config/systemd/user/

#podman generate systemd --name $POD_NAME_SVC > ~/.config/systemd/user/pod-${POD_NAME_SVC}.service
#systemctl enable pod-${POD_NAME_SVC}.service --user
podman generate systemd --files --name $ADCAPI_POD_NAME_SVC

# move file to user directory for systemd 
cp pod-${ADCAPI_POD_NAME_SVC}.service  ~/.config/systemd/user/pod-${ADCAPI_POD_NAME_SVC}.service
rm pod-${ADCAPI_POD_NAME_SVC}.service

cp container-maria_db${ADCAPI_DB_CONT_NAME_SUFFIX}.service \
          ~/.config/systemd/user/container-maria_db${ADCAPI_DB_CONT_NAME_SUFFIX}.service
rm container-maria_db${ADCAPI_DB_CONT_NAME_SUFFIX}.service

cp container-http${ADCAPI_DB_CONT_NAME_SUFFIX}.service \
          ~/.config/systemd/user/container-http${ADCAPI_DB_CONT_NAME_SUFFIX}.service
rm container-http${ADCAPI_DB_CONT_NAME_SUFFIX}.service

#cp container-keycloak${KEYCL_SVC_CONT_NAME_SUFFIX}.service \
#          ~/.config/systemd/user/container-keycloak${KEYCL_SVC_CONT_NAME_SUFFIX}.service
#rm container-keycloak${KEYCL_SVC_CONT_NAME_SUFFIX}.service

sleep 2
systemctl enable pod-${ADCAPI_POD_NAME_SVC}.service --user

log "systemd service files created and service enabled"

wait "processes to start" 4
echo " "
# --- pod top -----------------------------------------------------------------

# log "running processes in pod"
podman pod top $ADCAPI_POD_NAME_SVC

#log "stopping running pod ..."
#podman pod stop $KEYCL_POD_NAME_SVC

#wait "stopping running pod ... " 30

#log "starting systemd service ..."
#systemctl --user start pod-${KEYCL_POD_NAME_SVC}.service

#wait "service starting ..." 30

echo "adcapi service runs as a user service"
echo "check service : systemctl status pod-${ADCAPI_POD_NAME_SVC}.service --user"
echo "start service : systemctl start pod-${ADCAPI_POD_NAME_SVC}.service --user"
echo "stop service     : systemctl stop pod-${ADCAPI_POD_NAME_SVC}.service --user"
echo "disable service : systemctl disable pod-${ADCAPI_POD_NAME_SVC}.service --user"
echo "enable service : systemctl enable pod-${ADAPI_POD_NAME_SVC}.service --user"
echo 

#echo "end of installation, keycloak management interface is available on local port http://${KEYCL_HOST}:${KEYCL_POD_EX_PORT_HTTP}, you can find more information here https://www.keycloak.org/documentation.html"
#log "end of keycloak installation, next step install adc-middleware servcice"
log "end"

