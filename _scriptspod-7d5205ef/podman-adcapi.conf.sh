#!/bin/bash
# -----------------------------------------------------------------------------
# TOTO
# ..
# -----------------------------------------------------------------------------

#if (( $EUID != 0 )); then
    #echo "Please run with sudo.."
    #exit
#fi

# service admin e-mail
sysadmin_email="noreply@example.com";

# only used in development 
# HO vpn0 Home enp0s25 (you cannot use a device with an ip from 10/24 net because that is the podman network!!!)
# you cannot use lo or 127.0.0.1 here because that ip represents something different from within a pod
IP_LAN_DEVICE="vpn0"

# hostname 
host_name=$(hostname);

# path to backup directory, shared by all pods on host
# this pod will have its own dir inside that path 
PATH_BKUP_DIR="/var/data/bkup";

# if set to 1, bkup_file_rotation.sh will only use backup.daily folder
# by default, files are kept for 14 days in that folder, see script for details
BKUP_USES_ONLY_DIR_DAILY="1";

# -----------------------------------------------------------------------------
# keyloak and adc-middleware configuration
#GLOBAL_INSTANCE_SUFFIX="-LEGOLAS-dev-2022-0215"
GLOBAL_INSTANCE_SUFFIX="-LEGOLAS-adcapi-7d5205ef"

# in case your reverse proxy (like nginx) uses custom ca certificates, the files in list below
# will be copied and installed into middleware container  
GLOBAL_DO_UPDATE_CUSTOM_CERTIFICATES="false"

# list of custom root and intermediate certificates, which should be trusted in container
# files must be present in MIDWARE_CONFIG_FOLDER before installation and end with .crt (chmod 644) 
GLOBAL_CUSTOM_CERTIFICATES_LIST=( "intermediate.cert.pem.crt" "ca.cert.pem.crt")

# -----------------------------------------------------------------------------
# adc api pod configuration

# hostname where keycloak is available, if both pods are installed on same host and hostname
# resolution is not possible from middleware container, KEYCL_HOST:KEYCL_HOST_IP will be 
# added to pod, see install script line 122
ADCAPI_HOST="legolas" 

# ipv4 of keycloak host , used in middleware svc pod hosts file, is set below, cannot be 127.0.0.1 or ::1
ADCAPI_HOST_IP="127.0.0.1"

# used to identify individual pods and containers
ADCAPI_INSTANCE_SUFFIX=$GLOBAL_INSTANCE_SUFFIX

# suffix to specify the service  container insance e.g. TEST, DEV, PROD
ADCAPI_SVC_CONT_NAME_SUFFIX=$ADCAPI_INSTANCE_SUFFIX

# suffix to specify maria db container insance e.g. TEST, DEV, PROD
ADCAPI_DB_CONT_NAME_SUFFIX=$ADCAPI_INSTANCE_SUFFIX

# TAG for built svc  container https://hub.docker.com/r/jboss/keycloak/tags 
#KEYCL_SVC_CONT_TAG="15.0.0"
ADCAPI_SVC_CONT_TAG="1"

# TAG for built db container (mariadb) https://hub.docker.com/_/mariadb
ADCAPI_DB_CONT_TAG="10.3"

# service name for adcapi pod
ADCAPI_POD_NAME_SVC="adc-api${ADCAPI_INSTANCE_SUFFIX}"

# external port of keycloak pod http service
ADCAPI_POD_EX_PORT_HTTP="51431"

# password for mariadb db user postgres
ADCAPI_MARIADB_ROOT_PASSWORD="MegaP1ntM00ffins"

# host port of mariadb db
ADCAPI_POD_EX_PORT_DB="7306"

# specify the name of a database to be created on image startup
ADCAPI_MARIADB_DATABASE="testdb"

# file name (in /var/data/bkup/adc-api-data/conf)  will be copied to /etc/mysql/conf.d
ADCAPI_MARIADB_CUSTOM_CNF="my-custom.cnf";

# Set MARIADB_AUTO_UPGRADE to a non-empty value to have the entrypoint check whether \
# mysql_upgrade/mariadb-upgrade needs to run, and if so, run the upgrade before starting the MariaDB server.
# MARIADB_AUTO_UPGRADE="true"

# -----------------------------------------------------------------------------
# steps install script
# 1 - create 2nd super user
# 2 - create all dbs
# 3 - foreach db - add db user account, add sci db user 
# 4 - foreach study db import dump and add individual import file

# path for the top folder containing external data files
ADCAPI_MARIADB_DATA_PATH="/var/data/bkup/adc-api-data"; 

# path where initial db dumps are located compressed with bz2
ADCAPI_MARIADB_BZ2DUMP_PATH="/var/data/bkup/adc-api-data/sciReptor-dumps"; 

# internal maria db container path to sql dump files
ADCAPI_MARIADB_CONT_SQLDUMP_PATH="/mnt/data/sciReptor-dumps"; 

# super user for all maria dbs
ADCAPI_MARIADB_DBADMIN="dbadmin"

# password of super user
ADCAPI_MARIADB_DBADMIN_PWD="xxx5"

# user for maria scireptor dbs
ADCAPI_MARIADB_DBADMIN_SCI="sci"

# user passw for maria scireptor dbs
ADCAPI_MARIADB_DBADMIN_SCI_PWD="xxx"

# password of super user
ADCAPI_MARIADB_DBADMIN_PWD="xxx"

# list of databases to create during installation, each entry is also used as an account name to access that specific db
ADCAPI_MARIADB_DB_LIST=( "scireptor_meta" "scireptor_ontology" "scireptor_receptors" "athero_g07_09_rebased" "athero_w06_slice1_run4_rebased" "omega_w02_rebased" "omega_w05_rebased")
#ADCAPI_MARIADB_DB_LIST=( "scireptor_meta" "scireptor_ontology" "scireptor_receptors" "athero_g07_09_rebased" )

# dbs which represent a study repository, if conifgured an individual sql file can be imported on installation
ADCAPI_MARIADB_DBREPO_LIST=( "athero_g07_09_rebased" "athero_w06_slice1_run4_rebased" "omega_w02_rebased" "omega_w05_rebased")
#ADCAPI_MARIADB_DBREPO_LIST=( "athero_g07_09_rebased" )

# passwords for the individual db accounts, key is the db name
declare -A ADCAPI_MARIADB_DB_PWD_LIST_ASS;
ADCAPI_MARIADB_DB_PWD_LIST_ASS[scireptor_meta]="xxx";
ADCAPI_MARIADB_DB_PWD_LIST_ASS[scireptor_ontology]="xxx";
ADCAPI_MARIADB_DB_PWD_LIST_ASS[scireptor_receptors]="xxx";
ADCAPI_MARIADB_DB_PWD_LIST_ASS[athero_g07_09_rebased]="xxx";
ADCAPI_MARIADB_DB_PWD_LIST_ASS[athero_w06_slice1_run4_rebased]="xxx";
ADCAPI_MARIADB_DB_PWD_LIST_ASS[omega_w02_rebased]="xxx";
ADCAPI_MARIADB_DB_PWD_LIST_ASS[omega_w05_rebased]="xxx";

# list of db dumps to import foreach db on installation, all files are bzip2 compressed and end with .bz2
declare -A ADCAPI_MARIADB_DB_DUMPFILE_LIST_ASS;
ADCAPI_MARIADB_DB_DUMPFILE_LIST_ASS[scireptor_meta]="${ADCAPI_MARIADB_BZ2DUMP_PATH}/dump-scireptor_meta-2022-03-28_only-public-dbs.sql.bz2";
ADCAPI_MARIADB_DB_DUMPFILE_LIST_ASS[scireptor_ontology]="${ADCAPI_MARIADB_BZ2DUMP_PATH}/dump-scireptor_ontology-202110051822.sql.bz2";
ADCAPI_MARIADB_DB_DUMPFILE_LIST_ASS[scireptor_receptors]="${ADCAPI_MARIADB_BZ2DUMP_PATH}/dump-scireptor_receptors-202110051822.sql.bz2";
ADCAPI_MARIADB_DB_DUMPFILE_LIST_ASS[athero_g07_09_rebased]="${ADCAPI_MARIADB_BZ2DUMP_PATH}/dump-athero_g07_09_rebased-2022-03-10.sql.bz2";
ADCAPI_MARIADB_DB_DUMPFILE_LIST_ASS[athero_w06_slice1_run4_rebased]="${ADCAPI_MARIADB_BZ2DUMP_PATH}/dump-athero_w06_slice1_run4_rebased-2022-03-10.sql.bz2";
ADCAPI_MARIADB_DB_DUMPFILE_LIST_ASS[omega_w02_rebased]="${ADCAPI_MARIADB_BZ2DUMP_PATH}/dump-omega_w02_rebased-2022-03-10.sql.bz2";
ADCAPI_MARIADB_DB_DUMPFILE_LIST_ASS[omega_w05_rebased]="${ADCAPI_MARIADB_BZ2DUMP_PATH}/dump-omega_w05_rebased-2022-03-10.sql.bz2";

# list a file foreach study db, which should be imported on installation and place it here
# ${ADCAPI_MARIADB_DATA_PATH}/config
declare -A ADCAPI_MARIADB_DBREPO_IMPORTFILE_LIST_ASS;
ADCAPI_MARIADB_DBREPO_IMPORTFILE_LIST_ASS[athero_g07_09_rebased]="repertoire.view.sql";
ADCAPI_MARIADB_DBREPO_IMPORTFILE_LIST_ASS[athero_w06_slice1_run4_rebased]="repertoire.view.sql";
ADCAPI_MARIADB_DBREPO_IMPORTFILE_LIST_ASS[omega_w02_rebased]="repertoire.view.sql";
ADCAPI_MARIADB_DBREPO_IMPORTFILE_LIST_ASS[omega_w05_rebased]="repertoire.view.sql";


# container image to pull for http service
ADCAPI_HTTP_IMAGE="localhost/adc-api-alpine"

# image tag for http container 
ADCAPI_HTTP_IMAGE_TAG="7d5205ef"

# -----------------------------------------------------------------------------
# service account id running the pod 
POD_USER_ID="1000"

# existing file will trigger db backup
# created by load_ and update_ scripts
DO_BKUP_FLAG=${PATH_BKUP_DIR}/${ADCAPI_POD_NAME_SVC}/BKUP_DB.FLAG
# keycloaks backup path
#DO_BKUP_FLAG_KEYCL=${PATH_BKUP_DIR}/${KEYCL_POD_NAME_SVC}/BKUP_DB.FLAGi
# middlewares backup path
#DO_BKUP_FLAG_MIDWARE=${PATH_BKUP_DIR}/${MIDWARE_POD_NAME_SVC}/BKUP_DB.FLAG


# -----------------------------------------------------------------------------
SCRIPT_DIR=`dirname "$0"`

CURRENTUSER=$(who | awk 'NR==1{print $1}')
#echo $CURRENTUSER

LOGFILE_NAME="adcapi_${host_name}_${ADCAPI_POD_NAME_SVC}_.log"
if [ ! -e $SCRIPT_DIR/$LOGFILE_NAME  ];then
	touch $SCRIPT_DIR/$LOGFILE_NAME
	#chown $CURRENTUSER:$CURRENTUSER $SCRIPT_DIR/$LOGFILE_NAME
fi

function log () {
	local msg=$1
	echo $msg
	local dt=`date +%F_%T`
	echo "$dt $msg" >> $SCRIPT_DIR/$LOGFILE_NAME
}

function wait () {
	arg1=$1
	arg2=$2
	secs=$arg2
	#secs=$((5 * 60))
	while [ $secs -gt 0 ]; do
   		echo -ne "wait $arg2 secs (${arg1}) ... $secs\033[0K\r"
   		sleep 1
   		: $((secs--))
	done
}

function myip () {
	#IP_LAN_DEVICE="vpn0" see above
	current_IPv4_Lan=$(ip -o -4 addr list $IP_LAN_DEVICE | awk '{print $4}' | cut -d/ -f1)
	echo $current_IPv4_Lan
}

log "---"
echo "Using $(myip) as host ip."
ADCAPI_HOST_IP=$(myip)
ADCAPI_DB_IP=$(myip)
#MIDWARE_ADCREPO_IP=$(myip)
