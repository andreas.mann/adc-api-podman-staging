from argparse import ArgumentError
from sqlalchemy import and_, or_

field_map = {
    "repertoire": {
        "subject.species.id": "species_id",
        "subject.species.label": "label"
    }
}

def filter_query(query, data, Model):

    print("DEBUG filter.py -- QUERY:", data)
    if 'filters' in data:
        #
        # simple query with one op, field and value
        if data['filters']['op'] != 'and' and data['filters']['op'] != 'or':
            op = data['filters']['op']
            key = data['filters']['content']['field']
            value = data['filters']['content']['value']
            try:
                key = field_map[Model.__tablename__][key]
            except KeyError:
                print("DEBUG filter.py -- Did not replace key: ", key)
            if op == "=":
                filter_query.column = query.filter(
                    getattr(Model, key) == value
                ).all()
            elif op == ">":
                filter_query.column = query.filter(
                    getattr(Model, key) > value
                ).all()
            elif op == '<':
                filter_query.column = query.filter(
                    getattr(Model, key) < value
                ).all()
            elif op == '>=':
                filter_query.column = query.filter(
                    getattr(Model, key) >= value
                ).all()
            elif op == '<=':
                filter_query.column = query.filter(
                    getattr(Model, key) <= value
                ).all()
            elif op == '!=':
                filter_query.column = query.filter(
                    getattr(Model, key) != value
                ).all()
            elif op == 'like':
                filter_query.column = query.filter(
                    getattr(Model, key).like(value)
                ).all()
            elif op == 'not':
                filter_query.column = query.filter(
                    getattr(Model, key).not_(value)
                ).all()
            elif op == 'in':
                filter_query.column = query.filter(
                    getattr(Model, key).in_(value)
                ).all()
            elif op == 'is':
                filter_query.column = query.filter(
                    getattr(Model, key).is_(value)
                    ).all()
            elif op == 'contains':
                filter_query.column = query.filter(
                    getattr(Model, key).contains(value)
                ).all()
            elif op == 'exclude':
                filter_query.column = query.filter(
                    getattr(Model, key).notin_(value)
                ).all()
        #
        # Complex queries with multiple contents, fields and value with
        # and_ or or_ operators
        else:
            # TODO: (CB 2021-01-06) This assumes either AND or OR for all
            # conditions. Is this in line with ADC specs?
            if data['filters']['op'] == "and":
                join_operator = and_
            if data['filters']['op'] == "or":
                join_operator = or_
            conditions = []
            for filter in data['filters']['content']:
                op = filter['op']
                key = filter['content']['field']
                value = filter['content']['value']
                if op == '!=':
                    conditions.append(getattr(Model, key) != value)
                elif op == '>=':
                    conditions.append(getattr(Model, key) >= value)
                elif op == "=":
                    conditions.append(getattr(Model, key) == value)
                elif op == '<=':
                    conditions.append(getattr(Model, key) <= value)
                elif op == '>':
                    conditions.append(getattr(Model, key) > value)
                elif op == '<':
                    conditions.append(getattr(Model, key) < value)
                elif op == 'like':
                    conditions.append(getattr(Model, key).like(value))
                elif op == 'not':
                    conditions.append(getattr(Model, key).not_(value))
                elif op == 'in':
                    conditions.append(getattr(Model, key).in_(value))
                elif op == 'contains':
                    conditions.append(getattr(Model, key).contains(value))
                elif op == 'is':
                    conditions.append(getattr(Model, key).is_(value))
                elif op == 'exclude':
                    conditions.append(getattr(Model, key).notin_(value))
            filter_query.column = query.filter(
                join_operator(*conditions)
            ).all()
    else:
        filter_query.column = query


def filter_results(results, filters):
    filter_func = __build_filter_func(filters)
    results = [ result for result in results if filter_func(result) ]
    return results

def __build_filter_func(filters):
    op = filters.get('op', None)
    if op in ['and', 'or']:
        conditions = []
        for operand in filters['content']:
            conditions.append(__build_filter_func(operand))
        def func(row):
            for condition in conditions:
                if op == 'and':
                    if not condition(row): return False
                else:
                    if condition(row): return True
            return op == 'and'
    else:
        field = filters['content']['field']
        value = filters['content'].get('value', None)
        def func(row): 
            return __check_condition(op, QueryDict(row).get(field), value)

    return func

def __check_condition(op, value1, value2):
    operation = build_operator(op)
    if isinstance(value1, list):
        if op in ['=', 'like', 'contains', 'is not missing']: # inclusive operators
            for v in value1: 
                if operation(v, value2): 
                    return True
            return False
        elif op in ['!=', 'is missing']: # exclusive operators:
            for v in value1:
                if not operation(v, value2):
                    return False
            return True

    # not a list or unsupported operators:
    return  operation(value1, value2)

def build_operator(op):
    if op == '=':
        operation = lambda v1,v2: (float(v1) == float(v2)) if str(v1).isnumeric() else (v1 == v2)
    elif op == '!=':
        operation = lambda v1,v2: (float(v1) != float(v2)) if str(v1).isnumeric() else (v1 != v2)
    elif op == '<':
        operation = lambda v1,v2: (float(v1) < float(v2)) if str(v1).isnumeric() else (v1 < v2)
    elif op == '>':
        operation = lambda v1,v2: (float(v1) > float(v2)) if str(v1).isnumeric() else (v1 > v2)
    elif op == '<=':
        operation = lambda v1,v2: (float(v1) <= float(v2)) if str(v1).isnumeric() else (v1 <= v2)
    elif op == '>=':
        operation = lambda v1,v2: (float(v1) >= float(v2)) if str(v1).isnumeric() else (v1 >= v2)
    elif op == 'like':
        operation = lambda v1,v2: v1.like(v2)
    elif op == 'not':
        operation = lambda v1,v2: v1.not_(v2) # ? 
    elif op == 'in':
        operation = lambda v1,v2: v2 in v1
    elif op == 'is':
        operation = lambda v1,v2: v1.is_(v2) # ?
    elif op == 'is missing':
        operation = lambda v1,v2: v1 == None or v1 == [None]
    elif op == 'is not missing':
        operation = lambda v1,v2: v1 != None and v1 != [None]
    elif op == 'contains':
        operation = lambda v1,v2: v1.contains(v2)
    elif op == 'exclude':
        operation = lambda v1,v2: v2 not in v1
    elif op == 'and':
        operation = lambda v1,v2: v1 and v2
    elif op == 'or':
        operation = lambda v1,v2: v1 or v2

    else:
        raise ArgumentError()
    
    return operation

class QueryDict(dict):
# adapted from https://www.haykranen.nl/2016/02/13/handling-complex-nested-dicts-in-python/

    def get (self, path, default = None):
        keys = path.split('.')
        value = None

        for key in keys:
            if value:
                if isinstance(value, list):
                    #value = [ v.get(key, default) if v else None for v in value ]
                    new_value = []
                    for element in value:
                        if isinstance(element, list): #only 2 levels of nested lists supported (for now)
                            new_value += [ v.get(key, default) if v else None for v in element ]
                        elif element:
                            new_value.append(element.get(key, default))
                    value = new_value
                else:
                    value = value.get(key, default)
            else:
                value = dict.get(self, key, default)
            
            if not value:
                break;
        
        return value