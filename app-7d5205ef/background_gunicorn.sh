LOG_SUFFIX=$( date --utc +"%Y-%m-%dZ%H:%M:%S" )
nohup gunicorn --bind 0.0.0.0:51431 --workers=4 --ssl-version TLSv1_2 --certfile ../tls/fullchain.pem --keyfile ../tls/privkey.pem 'scireptor_api:create_app()' > ../log/gunicorn_${LOG_SUFFIX}_out.log 2> ../log/gunicorn_${LOG_SUFFIX}_err.log &
