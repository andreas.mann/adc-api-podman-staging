from flask_sqlalchemy import SQLAlchemy

db = SQLAlchemy()


class Sample(db.Model):
    __tablename__ = 'sample'
    sample_id = db.Column(
        db.Integer,
        primary_key=True
    )
    donor_id = db.Column(
        db.Integer,
        db.ForeignKey('donor.donor_id')
    )
    tissue = db.Column(
        db.String(45),
        index=False,
        unique=False,
        nullable=False
    )
    sampling_date = db.Column(
        db.DateTime,
        index=False,
        unique=False,
        nullable=True
    )
    add_sample_info = db.Column(
        db.String(100),
        index=False,
        unique=False,
        nullable=False
    )
    donor = db.relationship('Donor', backref='sample')


# CREATE VIEW `repertoire` AS SELECT DISTINCT
#    CONCAT(studies.study_id, ':', sort.sort_id) AS "repertoire_id",
#    sample.sample_id,
#    a.label AS "tissue",
#    sort.sort_id,
#    donor.donor_identifier,
#    donor.donor_id,
#    donor.strain,
#    studies.study_title,
#    donor.add_donor_info,
#    donor.species_id,
#    studies.study_id,
#    a.ontology_id AS "tissue_id",
#    studies.lab_name,
#    studies.lab_address,
#    studies.submitted_by,
#    studies.grants,
#    b.label AS "species_label",
#    studies.pub_ids,
#    studies.keywords_study,
#    studies.collected_by,
#    studies.database_name
#    FROM sort
#    INNER JOIN sample ON sample.sample_id = sort.sample_id
#    INNER JOIN donor ON donor.donor_id = sample.donor_id
#    INNER JOIN scireptor_ontology.ontologies a ON sample.tissue = a.ontology_id
#    INNER JOIN scireptor_ontology.ontologies b ON donor.species_id = b.ontology_id
#    INNER JOIN scireptor_meta.studies ON studies.database_name = DATABASE()
#    WHERE a.preferred_label = "1" AND b.preferred_label = "1" 
#    GROUP BY sort_id  ;

class Repertoire(db.Model):
    __tablename__ = 'repertoire'

    repertoire_id = db.Column(
        db.String(100),
        primary_key=True,
        index=True,
        unique=True,
        nullable=False,
    )
    sample_id = db.Column(
        db.Integer,
        index=True,
        unique=False,
        nullable=False
    )
    tissue = db.Column(
        db.String(100),
        index=True,
        unique=False,
        nullable=False
    )
    sort_id = db.Column(
        db.Integer,
        index=True,
        unique=True,
        nullable=False,
    )
    donor_identifier = db.Column(
        db.String(100),
        index=True,
        unique=False,
        nullable=False
    )
    donor_id = db.Column(
        db.Integer,
        index=True,
        unique=False,
        nullable=False,
    )
    strain = db.Column(
        db.String(100),
        index=True,
        unique=False,
        nullable=False
    )
    study_title = db.Column(
        db.String(100),
        index=True,
        unique=False,
        nullable=False
    )
    add_donor_info = db.Column(
        db.String(100),
        index=True,
        unique=False,
        nullable=False
    )
    species_id = db.Column(
        db.String(100),
        index=True,
        unique=False,
        nullable=False
    )
    study_id = db.Column(
        db.String(100),
        index=True,
        unique=False,
        nullable=False
    )
    tissue_id = db.Column(
        db.String(100),
        index=True,
        unique=False,
        nullable=False
    )
    lab_name = db.Column(
        db.String(100),
        index=True,
        unique=False,
        nullable=False
    )
    lab_address = db.Column(
        db.String(100),
        index=True,
        unique=False,
        nullable=False
    )
    submitted_by = db.Column(
        db.String(100),
        index=True,
        unique=False,
        nullable=False
    )
    grants = db.Column(
        db.String(100),
        index=True,
        unique=False,
        nullable=False
    )
    species_label = db.Column(
        db.String(100),
        index=True,
        unique=False,
        nullable=False
    )
    pub_ids = db.Column(
        db.String(100),
        index=True,
        unique=False,
        nullable=False
    )
    keywords_study = db.Column(
        db.String(100),
        index=True,
        unique=False,
        nullable=False
    )
    collected_by = db.Column(
        db.String(100),
        index=True,
        unique=False,
        nullable=False
    )

    database_name = db.Column(
        db.String(100),
        index=True,
        unique=False,
        nullable=False,
    )


class Flow(db.Model):
    __tablename__ = 'flow'
    __table_args__ = {'mysql_row_format': 'DYNAMIC'}
    flow_id = db.Column(db.Integer, primary_key=True)
    event_id = db.Column(
        db.Integer,
        db.ForeignKey('event.event_id')
    )
    channel_id = db.Column(
        db.Integer,
        db.ForeignKey('flow_meta.channel_id')
    )
    value = db.Column(db.Float, index=True, nullable=False)
    event = db.relationship('Event', backref='flow')
    flow_meta = db.relationship('FlowMeta', backref='flow')


class FlowMeta(db.Model):
    __tablename__ = 'flow_meta'
    __table_args__ = {'mysql_row_format': 'DYNAMIC'}
    channel_id = db.Column(db.Integer, primary_key=True)
    detector_name = db.Column(
        db.String(45),
        index=False,
        unique=False,
        nullable=False
    )
    marker_name = db.Column(
        db.String(45),
        index=False,
        unique=False,
        nullable=False
    )
    sort_id = db.Column(
        db.Integer,
        db.ForeignKey('sort.sort_id')
    )
    sort = db.relationship('Sort', backref='flow_meta')

class Sequence(db.Model):
    __tablename__ = 'sequences'
    __table_args__ = {'mysql_row_format': 'DYNAMIC'}
    seq_id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(45), index=True, unique=True, nullable=False)
    consensus_rank = db.Column(db.Integer, unique=True, nullable=False)
    locus = db.Column(
        db.String(1),
        index=True,
        unique=True,
        nullable=False
    )
    length = db.Column(
        db.Integer,
        index=True,
        unique=True,
        nullable=False
    )
    orient = db.Column(
        db.String(1),
        index=True,
        unique=True,
        nullable=False
    )
    igblast_productive = db.Column(
        db.Integer,
        unique=True,
        nullable=False
    )
    seq = db.Column(
        db.String(1000),
        index=True,
        unique=True,
        nullable=False
    )
    event_id = db.Column(
        db.Integer,
        db.ForeignKey('event.event_id')
    )
    event = db.relationship('Event', backref='sequences')

class Event(db.Model):
    __tablename__ = 'event'
    __table_args__ = {'mysql_row_format': 'DYNAMIC'}
    event_id = db.Column(db.Integer, primary_key=True)
    sort_id = db.Column(
        db.Integer,
        db.ForeignKey('sort.sort_id')
    )
    sort = db.relationship('Sort', backref='event')


class Sort(db.Model):
    __tablename__ = 'sort'
    sort_id = db.Column(db.Integer, primary_key=True)
    population = db.Column(
        db.String(45),
        index=True,
        unique=True,
        nullable=False
    )
    sample_id = db.Column(
        db.Integer,
        db.ForeignKey('sample.sample_id')
    )
    sample = db.relationship('Sample', backref='sort')


class SequenceInfo(db.Model):
    __tablename__ = 'sequencing_run'
    __table_args__ = {'mysql_row_format': 'DYNAMIC'}
    sequencing_run_id = db.Column(db.Integer, primary_key=True)
    experiment_id = db.Column(
        db.String(10),
        index=True,
        unique=True,
        nullable=False
    )
    name = db.Column(
        db.String(45),
        index=True,
        unique=True,
        nullable=False
    )


class Donor(db.Model):
    __tablename__ = 'donor'
    __table_args__ = {'mysql_row_format': 'DYNAMIC'}
    donor_id = db.Column(db.Integer, primary_key=True)
    donor_identifier = db.Column(
        db.String(45),
        index=True,
        unique=True,
        nullable=False
    )
    species_id = db.Column(
        db.String(20),
        index=True,
        unique=True,
        nullable=False
    )
    strain = db.Column(
        db.String(45),
        index=True,
        unique=True,
        nullable=False
    )
    add_donor_info = db.Column(
        db.String(2000),
        index=True,
        unique=True,
        nullable=False
    )


class CDR(db.Model):
    __tablename__ = 'CDR_FWR'
    __table_args__ = {'mysql_row_format': 'DYNAMIC'}
    CDR_FWR_id = db.Column(db.Integer, primary_key=True)
    seq_id = db.Column(
        db.Integer,
        db.ForeignKey('sequences.seq_id')
    )
    region = db.Column(
        db.String(20),
        index=True,
        unique=True,
        nullable=False
    )
    dna_seq = db.Column(
        db.String(300),
        index=True,
        unique=True,
        nullable=False
    )
    prot_seq = db.Column(
        db.String(100),
        index=True,
        unique=True,
        nullable=False
    )
    sequences = db.relationship('Sequence', backref='cdr')


class VDJ(db.Model):
    __tablename__ = 'VDJ_segments'
    __table_args__ = {'mysql_row_format': 'DYNAMIC'}
    VDJ_segments_id = db.Column(db.Integer, primary_key=True)
    seq_id = db.Column(
        db.Integer,
        db.ForeignKey('sequences.seq_id')
    )
    type = db.Column(
        db.String(1),
        index=True,
        unique=True,
        nullable=False
    )
    locus = db.Column(
        db.String(1),
        index=True,
        unique=True,
        nullable=False
    )
    igblast_rank = db.Column(
        db.Integer,
        index=True,
        unique=True,
        nullable=False
    )
    name = db.Column(
        db.String(20),
        index=True,
        unique=True,
        nullable=False
    )
    eval = db.Column(
        db.Float,
        index=True,
        unique=True,
        nullable=False
    )
    score = db.Column(
        db.Float,
        index=True,
        unique=True,
        nullable=False
    )
    VDJ_id = db.Column(
        db.Integer,
        index=True,
        unique=True,
        nullable=False
    )
    sequences = db.relationship('Sequence', backref='vdj')


class Receptor(db.Model):
    __tablename__ = 'IG_database'
    __bind_key__ = 'scireptor_receptors'
    __table_args__ = {'schema': 'scireptor_receptors'}
    receptor_id = db.Column(db.Integer, primary_key=True)
    IG_heavy = db.Column(
        db.String(2000),
        index=True,
        unique=True,
        nullable=False
    )
    IG_light = db.Column(
        db.String(2000),
        index=True,
        unique=True,
        nullable=False
    )
    IG_light_locus = db.Column(
        db.String(45),
        index=True,
        unique=True,
        nullable=False
    )
    # sequence = db.relationship('Sequence', backref='receptor')


class MetaData(db.Model):
    __tablename__ = 'studies'
    __bind_key__ = 'scireptor_meta'
    __table_args__ = {'schema': 'scireptor_meta'}
    study_id = db.Column(db.String(8), primary_key=True)
    database_name = db.Column(
        db.String(64),
        index=True,
        unique=True,
        nullable=False
    )
    study_title = db.Column(
        db.String(100),
        index=True,
        unique=False,
        nullable=True
    )
    grants = db.Column(
        db.String(100),
        index=True,
        unique=False
    )
    collected_by = db.Column(
        db.String(100),
        index=True,
        unique=False,
        nullable=False
    )
    lab_name = db.Column(
        db.String(100),
        index=True,
        unique=False
    )
    lab_address = db.Column(
        db.String(100),
        index=True,
        unique=False
    )
    submitted_by = db.Column(
        db.String(100),
        index=True,
        unique=False,
        nullable=False
    )
    keywords_study = db.Column(
        db.String(100),
        index=True,
        unique=False,
        nullable=False
    )
    pub_ids = db.Column(
        db.String(100),
        index=True,
        unique=False,
        nullable=False
    )
    # sample = db.relationship('Sample', backref='studies')


class Ontology(db.Model):
    __tablename__ = 'ontologies'
    __bind_key__ = 'scireptor_ontology'
    __table_args__ = {'schema': 'scireptor_ontology'}
    label = db.Column(db.String(100), primary_key=True)
    ontology_id = db.Column(
        db.String(100),
        index=True,
        unique=True,
        nullable=False
    )
    preferred_label = db.Column(db.Integer, index=True, nullable=False)
    # sample = db.relationship('Sample', backref='ontology')


# class Rearrangement(db.Model):
#     __tablename__ = 'rearrangement'
#     rearrangement_id = db.Column(db.Integer, primary_key=True)
#     v_call = db.Column(
#         db.String(100),
#         index=True,
#         unique=True,
#         nullable=False
#     )
#     d_call = db.Column(
#         db.String(100),
#         index=True,
#         unique=True,
#         nullable=False
#     )
#     j_call = db.Column(
#         db.String(100),
#         index=True,
#         unique=True,
#         nullable=False
#     )
#     c_call = db.Column(
#         db.String(100),
#         index=True,
#         unique=True,
#         nullable=False
#     )
#     locus = db.Column(
#         db.String(100),
#         index=True,
#         unique=True,
#         nullable=False
#     )
#     productive = db.Column(
#         db.String(100),
#         index=True,
#         unique=True,
#         nullable=False
#     )
#     cell_id = db.Column(
#         db.String(100),
#         index=True,
#         unique=True,
#         nullable=False
#     )
#     junction = db.Column(
#         db.String(2000),
#         index=True,
#         unique=True,
#         nullable=False
#     )
#     junction_aa = db.Column(
#         db.String(2000),
#         index=True,
#         unique=True,
#         nullable=False
#     )
