import ast
from collections import Counter
from itertools import groupby
import re
import flatdict
from contextlib import suppress
from flask import jsonify, json  #, request
# from sqlalchemy import text
# from sqlalchemy.sql.functions import coalesce, func
from timeit import default_timer as timer
# Local libraries
from models import *
from views import *
from filter import filter_results

info = {
    "title": "sciReptor ADC API",
    "description": "API response for any query",
    "version": 0.1,
    "contact":
    {
        "name": "sciReptor Team",
        "url": "https://gitlab.com/sciReptor"
    }
}

# shortcut for mapping possible UOs to labels without querying the DB,
# used in donor.add_donor_info's age_unit fields
# rf. https://www.ebi.ac.uk/ols/search?q=time&ontology=uo or http://purl.obolibrary.org/obo/UO_0000003
units_ontology = {
#    'UO:0000010': 'second',
#    'UO:0000031': 'minute',
#    'UO:0000032': 'hour',
    'UO:0000033': 'day',
    'UO:0000034': 'week',
    'UO:0000035': 'month',
    'UO:0000036': 'year',
#    'UO:0000151': 'century',
#    'UO:': '',
}

# dictionary cache for per-study pcr_targets
# key is the database name
pcr_targets_cache = {}

# dictionary cache for per-study cell counts
# key is the database name
cell_counts_cache = {}

def create_response_cell(cells, request_json):
    cell_list = []
    for cell_id, cell_records in groupby(cells, lambda c: c.cell_id):
        cells_list = list(cell_records)
        repertoire_ids = set([cell.repertoire_id for cell in cells_list])
        if len(repertoire_ids) > 1: print(f'DEBUG: Cell {cell_id} appears in {len(repertoire_ids)} repertoires!')
        repls_unique = ''.join(list(repertoire_ids))
        # markers = [
        #     cell.expression_marker for cell in cells_list
        # ]
        # values = [
        #     cell.expression_value for cell in cells_list
        # ]
        # merged_list = [({
        #     'expression_marker': markers[i],
        #     'expression_value': values[i]
        # }) for i in range(0, len(markers))]

        response_cell = {
            'cell_id': cell_id,
            'rearrangements': list(set([
                cell.rearrangement_id for cell in cells_list
                ])),
            'repertoire_id': repls_unique,
            # 'expression_tabular': merged_list,
            'virtual_pairing': bool(False)}
        cell_list.append(response_cell)
    
    if 'filters' in request_json:
        cell_list = filter_results(cell_list, request_json['filters'])

    if 'facets' in request_json:
        facets = __calculate_facets(cell_list, request_json[['facets']])
        return jsonify({'Info': info, 'Facet': facets})
    else:
        from_ = int(request_json.get('from', 0))
        size = int(request_json.get('size', len(cell_list)))

        if from_ > 0 or size < len(cell_list):
            cell_list = cell_list[from_:from_+size]

        if 'fields' in request_json:
            display_fields = request_json['fields']
            __filter_fields_with_whitelist(cell_list, display_fields)
    
    return jsonify({'Info': info, 'Cell': cell_list})


def create_response_rearrangement(rearrangements):
    rearrangement_ls = []
    for rearrangement in rearrangements:
        sub = {'rearrangement_id': rearrangement[0],
               "productive": rearrangement[3],
               "v_call": rearrangement[1],
               "d_call": rearrangement[2],
               "j_call": rearrangement[6],
               "c_call": rearrangement[4],
               "locus": "IG" + rearrangement[5],
               "cell_id": rearrangement[7],
               "junction": rearrangement[8],
               "junction_aa": rearrangement[9]}
        if sub['productive'] == 1:
            sub['productive'] = True
        else:
            sub['productive'] = False
        rearrangement_ls.append(sub)
    return jsonify({'Info': info, 'Rearrangement': rearrangement_ls})


def create_response_repertoire(repertoires, request_json):
    repertoire_ls = []

    for repertoire in repertoires:
        pcr_target = __get_pcr_target_from_db(repertoire.database_name)
        cell_counts = __get_cell_counts_from_db(repertoire.database_name)
        donor_info = __get_donor_info_as_dict(repertoire.add_donor_info) # [7]
        print("DEBUG - views.py:", donor_info)
        print("DEBUG - views.py:", repertoire.species_id) # [8]

        groups = {
            'repertoire_id': f'{repertoire.study_id}:{repertoire.sort_id}', # [2]
            'sample': [{
                'sample_id': f'{repertoire.study_id}:{repertoire.sample_id}', # [0]
                'tissue': {
                    'id': repertoire.tissue_id, # [10]
                    'label': repertoire.tissue # [1]
                },
                'pcr_target': pcr_target,
            }],
            'subject': {
                'subject_id': f'{repertoire.study_id}:{repertoire.donor_id}', # [4]
                'species': {
                    'id': repertoire.species_id, # [8]
                    'label': repertoire.species_label # [15]
                },
                'strain_name': repertoire.strain,
                'sex': donor_info['sex'],
                'age_max': donor_info['age_max'],
                'age_unit': {
                    'id': donor_info['age_unit'],
                    'label': units_ontology[donor_info['age_unit']], # OG 2022-01-24: take shortcut (could otherwise be queried from ontologies DB)
                },
                'age_min': donor_info['age_min'],
            },
            'study': {
                'study_id': str(repertoire.study_id), # [9]
                'study_title': repertoire.study_title, # [6]
                'grants': repertoire.grants, # [14]
                'lab_name': repertoire.lab_name, # [11]
                'lab_address': repertoire.lab_address, # [12]
                'submitted_by': repertoire.submitted_by, # [13]
                'pub_ids': repertoire.pub_ids, # [16]
                'keywords_study': repertoire.keywords_study.split(','), # [17]
                'collected_by': repertoire.collected_by, # [18]
            },
            'cellprocessing': {
                'cell_number': cell_counts[repertoire.sort_id]
            }
        }

        repertoire_ls.append(groups)

    if 'filters' in request_json:
        repertoire_ls = filter_results(repertoire_ls, request_json['filters'])

    if 'facets' in request_json:
        facets = __calculate_facets(repertoire_ls, request_json['facets'])
        return jsonify({'Info': info, 'Facet': facets})
    else:
        from_ = int(request_json.get('from', 0))
        size = int(request_json.get('size', len(repertoire_ls)))

        if from_ > 0 or size < len(repertoire_ls):
            repertoire_ls = repertoire_ls[from_:from_+size]

        if 'fields' in request_json:
            display_fields = request_json['fields']
            __filter_fields_with_whitelist(repertoire_ls, display_fields)
            
        return jsonify({'Info': info, 'Repertoire': repertoire_ls})

def __filter_fields_with_whitelist(obj, whitelist, current_path = ''):
    if isinstance(obj, dict):
        current_dict = obj.copy()
        for key, value in current_dict.items():
            elem_path = f'{current_path}.{key}'.lstrip('.')
            if not any(entry.startswith(elem_path) for entry in whitelist):
                with suppress(KeyError):
                    del obj[key]
            elif isinstance(value, (dict, list)) and not any(elem_path.startswith(entry) for entry in whitelist):
                __filter_fields_with_whitelist(obj[key], whitelist, elem_path)
    elif isinstance(obj, list):
        for elem in obj:
            if isinstance(elem, (dict, list)):
                __filter_fields_with_whitelist(elem, whitelist, current_path)

def __calculate_facets(dicts, facet_field):
    facet_values = []
    for objdict in dicts:
        d = flatdict.FlatterDict(objdict, delimiter='.')
        flatitems = d.items()
        for key, value in flatitems:
            key = re.sub('\.\d+', '', key)
            if key == facet_field:
                facet_values.append(value)
    counts = Counter(facet_values)
    facets = [{facet_field: k, "count" : v} for k,v in counts.items()]
    
    return facets

def create_response_receptor(receptors):
    receptor_ls = []
    for receptor in receptors:
        groups = {
            'receptor_id': receptor[0],
            'heavy_chain_aa': receptor[1],
            'light_chain_aa': receptor[2],
            'light_chain_locus': receptor[3]
        }
        receptor_ls.append(groups)
    return jsonify({'Info': info, 'Receptor': receptor_ls})


def create_response_facet(facets):
    return jsonify({'Info': info, 'Facet': facets})

def __get_donor_info_as_dict(field_info):
    #field_dict = ast.literal_eval(field_info) # Python literal
    field_dict = json.loads(field_info) # JSON string
    #field_dict = dict(pair.split('=', 1) for pair in field_info.split(',')) # comma-separated Key=Value pairs
    return field_dict

def __get_cell_counts_from_db(sql_database):
    if sql_database not in cell_counts_cache:
        cell_number_query = 'SELECT ' \
            'event.sort_id, sort.sample_id, COUNT(DISTINCT(event.event_id)) AS "count"' \
            f'FROM {sql_database}.event ' \
            f'JOIN {sql_database}.sort ON event.sort_id = sort.sort_id ' \
            'GROUP BY sort_id;'
        results_db = db.engine.execute(cell_number_query)

        cell_counts_cache[sql_database] = dict((result.sort_id, result.count) for result in results_db)

    return cell_counts_cache[sql_database]

def __get_pcr_target_from_db(sql_database):
    if sql_database not in pcr_targets_cache:
        query = f'SELECT DISTINCT add_pcr_info FROM {sql_database}.sequencing_run'
        results = db.engine.execute(query)
        targets = []
        for result in results:
            try:
                rec_dict = json.loads(result.add_pcr_info)
                targets += rec_dict['pcr_target']
            except:
                continue    
        distinct_targets = [ast.literal_eval(str_target) for str_target in set([str(py_target) for py_target in targets])] #rf. https://stackoverflow.com/a/11741999/9784 for explanation

        pcr_targets_cache[sql_database] = distinct_targets

    return pcr_targets_cache[sql_database]
