from flask import Flask, jsonify, request
from sqlalchemy import literal, select, null, create_engine
from sqlalchemy.orm import scoped_session, sessionmaker, Session
from sqlalchemy.sql.functions import coalesce, concat
from models import *
from views import *
from filter import filter_query, build_operator

engines = {}

# adapted from https://techspot.zzzeek.org/2012/01/11/django-style-database-routers-in-sqlalchemy#manual-access :
class RoutingSession(Session):
    def get_bind(self, mapper=None, clause=None):
        if self._name:
            return engines[self._name]
        else:
            return super().get_bind(mapper=mapper, clause=clause)

    _name = None
    def using_bind(self, name):
        session = RoutingSession()
        vars(session).update(vars(self))
        session._name = name
        return session

Session = scoped_session(sessionmaker(class_=RoutingSession, autocommit=True))

def create_app():
    app = Flask(__name__)
    app.config.from_json('default.cfg')
    sql_database = app.config['SQLALCHEMY_DATABASE_URI'].split('/')[3]

    binds = app.config['SQLALCHEMY_BINDS']
    for db_name, conn_str in binds.items():
        engines[db_name] = create_engine(conn_str)

    db.init_app(app)

    @app.route('/airr/v1', methods=['GET', 'POST'])
    def get_welcome():

        return {
            "result": "Success",
            "message": "Welcome to sciReptor Server"
        }

    @app.route('/airr/v1/info', methods=['GET', 'POST'])
    def get_info():

        return {
            "name": "sciReptor-python-MySQL",
            "description": "Single cell API",
            "version": "0.1.0"
        }

    @app.route('/airr/v1/repertoire', methods=['POST'])
    def post_repertoire():
        studies = db.session.query(MetaData.study_id, MetaData.database_name).distinct()

        repertoires = []
        for study in studies:
            study_repertoires = __query_database(study.database_name, Repertoire)
            repertoires += study_repertoires

        return create_response_repertoire(repertoires, request.get_json() if request.data else {})

    @app.route('/airr/v1/repertoire/<string:repertoire_id>', methods=['GET'])
    def get_repertoire(repertoire_id):
        (study_id, sort_id) = repertoire_id.split(':')
        db_name = __get_db_name(study_id)
        results = __query_database(db_name, Repertoire).filter(Repertoire.study_id == study_id, Repertoire.sort_id == sort_id)
        return create_response_repertoire(results, request.get_json() if request.data else {})

    @app.route('/airr/v1/cell', methods=['POST'])
    def post_cell():
        studies = db.session.query(MetaData.study_id, MetaData.database_name).distinct()

        filter_study_id = None
        filter_op = None
        req_data = request.get_json() if request.data else {}
        
        #check for single-condition filter and prepare it (e.g. extract study_id):
        if 'filters' in req_data:
            filters = req_data['filters']
            if filters['op'] not in ['and', 'or']:
                filter_value = filters['content']['value']
                if ':' in filter_value:
                    (filter_study_id, filter_value) = filter_value.split(':')
                filter_field = filters['content'] ['field']
                if filter_field != 'rearrangements':
                    filter_op = build_operator(filters['op'])
                    del req_data['filters']
            #else: 
                # multi-condition filters will be handled post-query

        if filter_study_id is not None:
            # if a filter referenced a specific study_id, only query that study(/database):
            studies = studies.filter(MetaData.study_id == filter_study_id)

        cells = []
        for study in studies:
            study_cells = __query_cells(study.study_id, study.database_name)
            
            # apply pre-query filters
            if filter_op:
                if ((filter_study_id is None) or (filter_study_id == study.study_id)):
                    if filter_field == 'cell_id':
                        study_cells = study_cells.filter(filter_op(Event.event_id, filter_value))
                    if filter_field == 'repertoire_id':
                        study_cells = study_cells.filter(filter_op(Event.sort_id, filter_value))
                    #if filter_field == 'rearrangements': 
                        # will be handled post-query

            cells += study_cells

        return create_response_cell(cells, req_data)

    @app.route('/airr/v1/cell/<string:cell_id>', methods=['GET'])
    def get_cell(cell_id):
        (study_id, study_cell_id) = cell_id.split(':')
        cell_query = __query_cells(study_id)\
            .filter(Event.event_id == study_cell_id)

        return create_response_cell(cell_query.all(), request.get_json() if request.data else {})

    @app.route('/airr/v1/rearrangement', methods=['POST'])
    def post_rearrangement():
        rearrangement_query = db.session.query(
            Rearrangement.rearrangement_id,
            Rearrangement.v_call,
            Rearrangement.d_call,
            Rearrangement.productive,
            Rearrangement.c_call,
            Rearrangement.locus,
            Rearrangement.j_call,
            Rearrangement.cell_id,
            Rearrangement.junction,
            Rearrangement.junction_aa)\
            .filter(CDR.region == "CDR3")\
            .order_by(Rearrangement.cell_id)\
            .distinct()
        print(rearrangement_query)
        if request.data:
            data = request.get_json()
            if 'facets' in data:
                try:
                    field = data['filters']['content']['field']
                    value = data['filters']['content']['value']
                    query = 'SELECT ' \
                            'rearrangement.{}, ' \
                            'COUNT(rearrangement.{}) ' \
                        'FROM healthy.rearrangement '\
                        'WHERE rearrangement.{} =  \"{}\" ' \
                        'GROUP BY rearrangement.{}'\
                        .format(field, field, field, value, field)
                except KeyError:
                    field = data['facets']
                    query = 'SELECT ' \
                            'rearrangement.{}, ' \
                            'COUNT(rearrangement.{}) ' \
                        'FROM healthy.rearrangement '\
                        'GROUP BY rearrangement.{}'\
                        .format(field, field, field)
                engine_db1 = db.get_engine(app, 'db1')
                results_db1 = engine_db1.execute(query)
                facets_response = []
                for r in results_db1:
                    sub = {
                        data['facets']: r[0],
                        "count": r[1]
                    }
                    facets_response.append(sub)
                return create_response_facet(facets_response)
            else:
                filter_query(rearrangement_query, data, Rearrangement)
                return create_response_rearrangement(filter_query.column)
        else:
            return create_response_rearrangement(rearrangement_query)

    @app.route('/airr/v1/rearrangement/<int:re_id>', methods=['GET'])
    def get_rearrangement(re_id):
        rearrangement_query = db.session.query(
            Rearrangement.rearrangement_id,
            Rearrangement.v_call,
            Rearrangement.d_call,
            Rearrangement.productive,
            Rearrangement.c_call,
            Rearrangement.locus,
            Rearrangement.j_call,
            Rearrangement.cell_id,
            Rearrangement.junction,
            Rearrangement.junction_aa)\
            .filter(CDR.region == "CDR3")\
            .filter(Rearrangement.rearrangement_id == re_id)\
            .distinct()
        print(rearrangement_query)
        return create_response_rearrangement(rearrangement_query)

    @app.route('/airr/v1/receptor', methods=['POST'])
    def post_receptor():
        query_receptor = db.session.query(
            Receptor.receptor_id,
            Receptor.IG_heavy,
            Receptor.IG_light,
            Receptor.IG_light_locus
        ).all()
        if request.data:
            data = request.get_json()
            filter_query(query_receptor, data, Receptor)
            return create_response_receptor(filter_query.column)
        else:
            return create_response_receptor(query_receptor)

    def __get_db_name(study_id):
        meta_query = db.session.query(MetaData.database_name).filter(MetaData.study_id == study_id).first()
        return meta_query[0]

    def __query_database(database_name, what):
        session = Session().using_bind(database_name)
        query = session.query(what)
        return query

    def __query_cells(study_id, database_name = None):
        if database_name is None:
            database_name = __get_db_name(study_id)
        session = Session().using_bind(database_name)
        query = session.query(
                concat(study_id, ':', Event.event_id).label('cell_id'),
                concat(study_id, ':', Sequence.seq_id).label('rearrangement_id'),
                concat(study_id, ':', Sort.sort_id).label('repertoire_id')
                # Flow.event_id,
                # Flow.value.label('expression_value'),
                # coalesce(FlowMeta.marker_name, FlowMeta.detector_name).label('expression_marker'),
                # literal(study.study_id).label('study_id')
            ).join(Sort, Event.sort_id == Sort.sort_id)\
                .join(Sequence, Event.event_id == Sequence.event_id)\
                .order_by(Sequence.event_id)\
                .join(Flow, Flow.event_id == Sequence.event_id)\
                .join(FlowMeta, Flow.channel_id == FlowMeta.channel_id)
        return query
    
    return app
